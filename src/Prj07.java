public class Prj07 {

    public static void main(String[] args) {

         A a = new B();
         a.operate();

    }
}

abstract class A{
   abstract void operate();
}

class B extends A{

    @Override
    void operate() {
        System.out.println("Do something...");
    }
}

